<?php

use Illuminate\Database\Seeder;
use App\Models\Area;

class AreaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $area =  new Area();

        $area->create(['name' => 'Panjer', 'slug' => 'Panjer']);
        $area->create(['name' => 'Renon', 'slug' => 'Renon']);
        $area->create(['name' => 'Kesiman', 'slug' => 'Kesiman']);
    }
}
