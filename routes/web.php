<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/dashboard');
});

// Route::get('/', function () {
//     return redirect('/dashboard');
// });

Auth::routes(['register' => false]);

Route::get('/dashboard', 'HomeController@index')->name('dashboard');

Route::middleware(['auth', 'role:superadmin'])->namespace('Superadmin')->prefix('superadmin')->name('superadmin.')->group(function () {
    Route::prefix('admin')->name('admin.')->group(function () {
        Route::get('/', 'AdminController@index')->name('index');
        Route::get('/create', 'AdminController@create')->name('create');
        Route::post('/create', 'AdminController@store')->name('store');
        Route::get('/edit/{admin:username}', 'AdminController@edit')->name('edit');
        Route::post('/edit/{admin}', 'AdminController@update')->name('update');
        Route::post('/delete', 'AdminController@delete')->name('delete2');
        Route::get('/delete/{admin}', 'AdminController@destroy')->name('admin.destroy');
    });

    Route::prefix('staff')->name('staff.')->group(function (){
        Route::get('/','StaffController@index')->name('index');
        Route::get('/create' , 'StaffController@create')->name('create');
        Route::post('/store','StaffController@store')->name('store');
        Route::get('/edit/{staff:username}' , 'StaffController@edit')->name('edit');
        Route::post('/update/{staff}' , 'StaffController@update')->name('update');
        Route::post('/delete2' , 'StaffController@delete2')->name('delete2');
        Route::get('/destroy/{staff}' , 'StaffController@destroy')->name('destroy');
    });
});
