<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StaffFormRequest;
use App\User;
use App\Models\Area;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class StaffController extends Controller
{
    public function index()
    {
        $staffs = User::role('staff')->get();
        return view('superadmin.staff.index' , compact('staffs'));
    }

    public function create()
    {
        $areas = Area::all();
        return view('superadmin.staff.create', compact('areas'));
    }

    public function store(StaffFormRequest $request)
    {
        $data = [
            'name' => $request->name,
            'username' => $request->username,
            'password' => bcrypt($request->password),
            'email' => $request->email,
            'address' => $request->address,
            'phone' => $request->phone,
            'gender' => $request->gender,
            'area_id' => $request->area,
            'nik' => $request->nik,
        ];

        $staff = User::create($data);
        $staff->assignRole('staff');

        $message = 'Berhasil menambah staff baru';
        Session::flash('staff' , $message);

        return $this->index();

    }

    public function edit(User $staff)
    {
        $areas = Area::all();
        return view('superadmin.staff.edit',compact('staff','areas'));
    }

    public function update(Request $request, User $staff)
    {
             if ($request->password == ' ') {
                  $password = $staff->password;
             } else {
                  $password = bcrypt($request->password);
             }

             $data = [
                'name' => request()->name,
                'username' => request()->username,
                'password' => $password,
                'email' => request()->email,
                'address' => request()->address,
                'phone' => request()->phone,
                'gender' => request()->gender,
                'nik' => request()->nik,
                'area_id' => $request->area
            ];

            $staff->update($data);

            $message = 'Berhasil Mengubah Staff';
            Session::flash('staff', $message);

            return $this->index();
    }


    public function delete2()
    {
        $a = request()->id;

        $staff = User::find($a);
        $staff->delete();

        $admins = User::role('admin')->get();
        $rs['content'] = View::make('superadmin.admin.partial.content')
            ->with('admins', $admins)
            ->render();

        return response($rs);
    }

    public function destroy(User $staff)
    {
        $staff->delete();
        $message = 'Berhasil Menghapus Staff';
        Session::flash('staff', $message);
        return $this->index();
    }

}
