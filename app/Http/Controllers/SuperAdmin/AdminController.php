<?php

namespace App\Http\Controllers\Superadmin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Http\Requests\AdminFormRequest;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = User::role('admin')->get();
        return view('superadmin.admin.index', compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return (view('superadmin.admin.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminFormRequest $request)
    {
        $data = [
            'name' => request()->name,
            'username' => request()->username,
            'password' => bcrypt(request()->password),
            'email' => request()->email,
            'address' => request()->address,
            'phone' => request()->phone,
            'gender' => request()->gender,
            'nik' => request()->nik,

        ];

        $staff = User::create($data);
        $staff->assignRole('admin');

        $message = 'Berhasil menambahkan Admin baru';
        Session::flash('admin', $message);

        return redirect(route('superadmin.admin.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $admin)
    {

        return view('superadmin.admin.edit', compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $admin)
    {
        if ($request->password == '') {
            $password = $admin->password;
        } else {
            $password = bcrypt($request->password);
        }

        $data = [
            'name' => request()->name,
            'username' => request()->username,
            'password' => $password,
            'email' => request()->email,
            'address' => request()->address,
            'phone' => request()->phone,
            'gender' => request()->gender,
            'nik' => request()->nik,

        ];

        $admin->update($data);

        $message = 'Berhasil Mengubah Admin';

        Session::flash('admin', $message);

        return redirect(route('superadmin.admin.index'));
    }
    public function destroy(User $admin)
    {
        $admin->delete();

        $message = 'Berhasil Menghapus Admin';

        Session::flash('admin', $message);

        return redirect(route('superadmin.admin.index'));
    }

    public function delete()
    {
        $a = request()->id;

        $staff = User::find($a);
        $staff->delete();

        $staffs = User::role('staff')->get();
        $rs['content'] = View::make('superadmin.staff.partial.content')
            ->with('staffs', $staffs)
            ->render();

        return response($rs);
    }
}
