<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SignInController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        if(!$token = auth()->attempt($request->only('username', 'password'))){
            return response()->json([
                'errors' => [
                    'email' => [
                        'Could not sign you in with those deatils '
                    ]
                ]
                    ], 422);
        }

        return response()->json([
            'data' => compact('token')
        ]);
    }
}
